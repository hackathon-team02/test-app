# Python Webapp Demo

kubectl create namespace test-app

argocd login argocd-test.bravofly.intra --username admin --grpc-web

argocd app create test-app \
--repo https://gitlab.com/hackathon-team02/test-app-chart.git \
--path . \
--dest-server https://kubernetes.default.svc \
--dest-namespace test-app \
--sync-policy automated \
--auto-prune \
--self-heal
