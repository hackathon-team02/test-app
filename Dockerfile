FROM openresty/openresty:1.19.9.1-12-alpine-apk

COPY www/ /usr/local/openresty/nginx/html/

EXPOSE 80